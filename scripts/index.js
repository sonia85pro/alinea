// const fichiersList = document.querySelector('.fichier');

// //setup fichier
// const setupFichier = (data) =>{
//    let html ='';
//    data.forEach(doc => {
//        const fichiers = doc.data();
//        const li = `
//       <li>
          
//          < div class = "collapsible-header grey lighten-4" > ${fichier.contient}/div> 
//          < div class = "collapsible-body white" > ${fichier.fichierData}</div >
//          </li>
//       `;
//       html += li;
// //console.log(fichier);
//    }); 
//    fichiersList.innerHTML = html;
// };






// // setup materialize components
// document.addEventListener('DOMContentLoaded', function () {

//     var modal = document.querySelectorAll('.modals');
//     M.Modals.init(modal);

//     var items = document.querySelectorAll('.collapsible');
//     M.Collapsible.init(items);

// });
// 
// DOM elements
const guideList = document.querySelector('.guides');
const loggedOutLinks = document.querySelectorAll('.logged-out');
const loggedInLinks = document.querySelectorAll('.logged-in');
const accountDetails = document.querySelector('.account-details');
const adminItems = document.querySelectorAll('.admin');

const setupUI = (user) => {
    if (user) {
        if (user.admin) {
            adminItems.forEach(item => item.style.display = 'block');
        }
        // account info
        db.collection('users').doc(user.uid).get().then(doc => {
            const html = `
        <div>Logged in as ${user.email}</div>
        <div>${doc.data().bio}</div>
        <div class="pink-text">${user.admin ? 'Admin' : ''}</div>
      `;
            accountDetails.innerHTML = html;
        });
        // toggle user UI elements
        loggedInLinks.forEach(item => item.style.display = 'block');
        loggedOutLinks.forEach(item => item.style.display = 'none');
    } else {
        // clear account info
        accountDetails.innerHTML = '';
        // toggle user elements
        adminItems.forEach(item => item.style.display = 'none');
        loggedInLinks.forEach(item => item.style.display = 'none');
        loggedOutLinks.forEach(item => item.style.display = 'block');
    }
};

// setup guides
const setupGuides = (data) => {

    if (data.length) {
        let html = '';
        data.forEach(doc => {
            const guide = doc.data();
            const li = `
        <li>
          <div class="collapsible-header grey lighten-4"> ${guide.title} </div>
          <div class="collapsible-body white"> ${guide.content} </div>
        </li>
      `;
            html += li;
        });
        guideList.innerHTML = html;
    } else {
        guideList.innerHTML = '<h5 class="center-align">Connectez-vous pour voir les présences et absences des Dev Web & Data</h5>';
    }


};

// setup materialize components
document.addEventListener('DOMContentLoaded', function () {

    var modals = document.querySelectorAll('.modal');
    M.Modal.init(modals);

    var items = document.querySelectorAll('.collapsible');
    M.Collapsible.init(items);

});